/*

    SOLUTION

    You cannot just copy and paste this because
    the bucket name will need to be your bucket name

    If you run it "as is" it will not work!

    You must replaace <FMI> with your bucket name

    e.g

    2019-03-20-ricky-catlostandfoundwebsite

    Keeo the quotes in there below, and literally just 
    replace the characters <FMI>


*/
var 
    AWS = require("aws-sdk"),
    S3API = new AWS.S3({}),
    FS = require("fs"),
    bucket_name_str = "<FMI>";


function uploadItemAsBinary(key_name_str, content_type_str, bin){
    var params = {
        Bucket: bucket_name_str,
        Key: key_name_str,
        Body: bin,
        ContentType: content_type_str,
        CacheControl: "max-age=0"
    };
    S3API.putObject(params, function(err, data){
        console.log(err, data);
    });
}

(function init(){
    var cat_pic_bin = FS.readFileSync("catwebsite/cat.jpg");
    uploadItemAsBinary("cat.jpg", "image/jpg", cat_pic_bin);
    var index_page_bin = FS.readFileSync("catwebsite/index.html");
    uploadItemAsBinary("index.html", "text/html", index_page_bin);
})();